<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'ivan',
                'email' => 'a@a.com',
                'city' => 'Mostar',
                'address' => 'Ante St',
                'email_verified_at' => NULL,
                'admin' => 0,
                'password' => '$2y$10$FN4FCqwJnh94n/aXThmgZOTyzgtlYJuQ.G/27rhc6Rsk/FkArI.ui',
                'remember_token' => NULL,
                'created_at' => '2020-05-05 20:34:01',
                'updated_at' => '2020-05-05 20:34:01',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'filip',
                'email' => 'b@b.com',
                'city' => 'Sarajevo',
                'address' => 'Ante St',
                'email_verified_at' => NULL,
                'admin' => 0,
                'password' => '$2y$10$CPE4s6WCLP/AvOi8jUVkQOX6SP/goRtrD7yP3au2QcIewfjnANYfS',
                'remember_token' => NULL,
                'created_at' => '2020-05-05 20:34:29',
                'updated_at' => '2020-05-05 20:34:29',
            ),
        ));
        
        
    }
}