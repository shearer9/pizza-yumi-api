<?php

use Illuminate\Database\Seeder;

class PizzasTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('pizzas')->delete();
        
        \DB::table('pizzas')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Neaoplitan',
                'description' => 'Neapolitan is the original pizza. This delicious pie dates all the way back to 18th century in Naples, Italy',
                'popular' => 0,
                'pizza_image' => '/images/pizzas/neaoplitan_1588873709.jpg',
                'sm_price' => 1200,
                'md_price' => 1100,
                'lg_price' => 1200,
                'created_at' => '2020-05-07 19:34:04',
                'updated_at' => '2020-05-07 17:48:29',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Marinara',
                'description' => 'Pizza Marinara: Features tomatoes, garlic, oregano, and extra virgin olive oil.',
                'popular' => 0,
                'pizza_image' => '/images/pizzas/marinara_1588873832.jpg',
                'sm_price' => 1000,
                'md_price' => 950,
                'lg_price' => 1000,
                'created_at' => '2020-05-07 19:34:04',
                'updated_at' => '2020-05-07 17:50:32',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Chicago ',
                'description' => 'The toppings for Chicago pizza are ground beef, sausage, pepperoni, onion, mushrooms, and green peppers, placed underneath the tomato sauce',
                'popular' => 0,
                'pizza_image' => NULL,
                'sm_price' => 1200,
                'md_price' => 1250,
                'lg_price' => 1400,
                'created_at' => '2020-05-07 19:34:03',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'New York-Style Pizza',
                'description' => 'With its characteristic large, foldable slices and crispy outer crust, New York-style pizza is one of America’s most famous regional pizza types',
                'popular' => 0,
                'pizza_image' => NULL,
                'sm_price' => 750,
                'md_price' => 865,
                'lg_price' => 940,
                'created_at' => '2020-05-07 19:33:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Sicilian Pizza',
                'description' => 'Sicilian pizza, also known as "sfincione," provides a thick cut of pizza with pillowy dough, a crunchy crust, and robust tomato sauce',
                'popular' => 0,
                'pizza_image' => NULL,
                'sm_price' => 1425,
                'md_price' => 1500,
                'lg_price' => 1600,
                'created_at' => '2020-05-07 19:34:05',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Greek Pizza',
                'description' => 'Greek pizza was created by Greek immigrants who came to America and were introduced to Italian pizza',
                'popular' => 0,
                'pizza_image' => NULL,
                'sm_price' => 780,
                'md_price' => 880,
                'lg_price' => 940,
                'created_at' => '2020-05-07 19:34:06',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'California Pizza',
                'description' => 'California pizza, or gourmet pizza, is known for its unusual ingredients. This pizza got its start back in the late 1970’s when Chef Ed LaDou began experimenting with pizza recipes in the classic Italian restaurant, Prego.',
                'popular' => 0,
                'pizza_image' => NULL,
                'sm_price' => 1000,
                'md_price' => 1040,
                'lg_price' => 1240,
                'created_at' => '2020-05-07 19:34:06',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'Detroit Pizza',
                'description' => 'Reflecting the city’s deep ties to the auto industry, Detroit-style pizza was originally baked in a square automotive parts pan in the 1940’s. Detroit pizza is first topped with pepperoni',
                'popular' => 0,
                'pizza_image' => NULL,
                'sm_price' => 930,
                'md_price' => 935,
                'lg_price' => 1035,
                'created_at' => '2020-05-07 19:34:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}