<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['middleware'=>'auth:sanctum'], function() {
    Route::get('/user', function (Request $request) {
        return $request->user();
    });
    Route::post('pizzas/{pizza}', 'PizzaController@update');
    Route::resource('pizzas', 'PizzaController')->except(['index']);
    Route::resource('orders', 'OrderController');
    Route::post('logout', 'Auth\LoginController@logout');

});
Route::get('pizzas', 'PizzaController@index');
Route::post('login', 'Auth\LoginController@login');


