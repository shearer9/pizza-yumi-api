<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    protected $fillable = ['pizza_id', 'size'];


    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function pizza()
    {
        return $this->belongsTo(Pizza::class);
    }

    public function size()
    {
        return $this->belongsTo(Size::class);
    }

    public function priceOfPizza()
    {
        if ($this->size == 'Small')
            return $this->pizza()->value('sm_price');
        if ($this->size == 'Medium')
            return $this->pizza()->value('md_price');
        if ($this->size == 'Large')
            return $this->pizza()->value('lg_price');
    }
}
