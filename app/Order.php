<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [];

    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function orderDetails()
    {
        return $this->hasMany(OrderDetail::class);
    }

    public function calculateTotal()
    {
        $sum = 0;
        $orderDetails = $this->orderDetails->load('pizza');
        foreach ($orderDetails as $orderDetail) {
            $sum+= $orderDetail->priceOfPizza();
        }
        $this->total_price = $sum;
        $this->save();
    }
}

