<?php

namespace App;

use App\Traits\UploadTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class Pizza extends Model
{
    use SoftDeletes;
    use UploadTrait;

    protected $fillable = ['name', 'description', 'popular', 'pizza_image', 'sm_price', 'md_price', 'lg_price'];

    public function getSmPriceAttribute($value)
    {
        return $value / 100;
    }

    public function getMdPriceAttribute($value)
    {
        return $value / 100;
    }

    public function getLgPriceAttribute($value)
    {
        return $value / 100;
    }

    public function setSmPriceAttribute($value)
    {
        $this->attributes['sm_price'] = $value * 100;
    }

    public function setMdPriceAttribute($value)
    {
        $this->attributes['md_price'] = $value * 100;
    }

    public function setLgPriceAttribute($value)
    {
        $this->attributes['lg_price'] = $value * 100;
    }

    public function sizes()
    {
        return $this->hasMany(Size::class);
    }

    public function orderDetails()
    {
        return $this->hasMany(OrderDetail::class);
    }

    public function addImage($image)
    {
        $name = Str::slug($this->name) . '_' . time();
        $folder = '/images/pizzas/';
        $filePath = $folder . $name . '.' . $image->getClientOriginalExtension();
        $this->uploadOne($image, $folder, 'public', $name);
        $this->pizza_image = $filePath;
    }

}
