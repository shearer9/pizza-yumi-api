<?php

namespace App\Http\Controllers;

use App\Pizza;
use App\Traits\UploadTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class PizzaController extends Controller
{
    use UploadTrait;

    public function __construct()
    {
        $this->middleware('admin', ['only' => ['store', 'update', 'destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->query('popular'))
            return Pizza::where('popular', true)->get();
        $pizzas = Pizza::all();
        return $pizzas;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(), [
            'name' => 'required|string',
            'description' => 'required|string',
            'popular' => 'required|boolean',
            'sm_price' => 'required|numeric',
            'md_price' => 'required|numeric',
            'lg_price' => 'required|numeric',
            'pizza_image' => 'nullable|image|mimes:jpeg,png,jpg|max:2048'
        ]);

        $pizza = Pizza::make(request([
            'name',
            'description',
            'popular',
            'sm_price',
            'md_price',
            'lg_price'
        ]));

        if ($request->has('pizza_image')) {
            $image = $request->file('pizza_image');
            $pizza->addImage($image);
        }
        $pizza->save();
        return $pizza;
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Pizza $pizza
     * @return \Illuminate\Http\Response
     */
    public function show(Pizza $pizza)
    {
        return $pizza;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Pizza $pizza
     * @return \Illuminate\Http\Response
     */
    public function edit(Pizza $pizza)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Pizza $pizza
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pizza $pizza)
    {
        $this->validate(request(), [
            'name' => 'required|string',
            'description' => 'required|string',
            'popular' => 'required|integer',
            'sm_price' => 'required|numeric',
            'md_price' => 'required|numeric',
            'lg_price' => 'required|numeric',
            'pizza_image' => 'nullable|image|mimes:jpeg,png,jpg|max:2048'
        ]);

        $pizza->update(request([
            'name',
            'description',
            'popular',
            'sm_price',
            'md_price',
            'lg_price'
        ]));
        
        if ($request->filled('pizza_image')) {
            $image = $request->file('pizza_image');
            $pizza->addImage($image);
        }

        $pizza->save();
        return response()->json($pizza, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Pizza $pizza
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pizza $pizza)
    {
        return $pizza->delete();
    }
}
