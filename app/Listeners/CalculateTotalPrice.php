<?php

namespace App\Listeners;

use App\Events\CreatedNewOrder;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class CalculateTotalPrice
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CreatedNewOrder  $event
     * @return void
     */

    // Here we can send confirmation mail or something else related for purchase
    public function handle(CreatedNewOrder $event)
    {
        $order = $event->order;
        $order->calculateTotal();
    }
}
